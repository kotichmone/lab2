﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimanovichLab2
{
    class Threat
    {
        public double id;
        public string name;
        public string value;
        public string Source { get; set; }
        public string ObjectThreat { get; set; }
        public bool Сonfidentiality { get; set; }
        public bool Integrity { get; set; }
        public bool Availability { get; set; }
        public string Datastart { get; set; }
        public string Dataupdate { get; set; }

        public Threat(double id, string name, string value, string source, string objectThreat, bool сonfidentiality, bool integrity, bool availability, string datastart, string dataupdate)
        {
            this.id = id;
            this.name = name;
            this.value = value;
            Source = source;
            ObjectThreat = objectThreat;
            Сonfidentiality = сonfidentiality;
            Integrity = integrity;
            Availability = availability;
            Datastart = datastart;
            Dataupdate = dataupdate;
        }
    }
}
