﻿namespace SimanovichLab2
{
    partial class FormBDU
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFSTEK = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.textBoxresult = new System.Windows.Forms.TextBox();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.gridBDU = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBDU)).BeginInit();
            this.SuspendLayout();
            // 
            // btnFSTEK
            // 
            this.btnFSTEK.Location = new System.Drawing.Point(599, 13);
            this.btnFSTEK.Name = "btnFSTEK";
            this.btnFSTEK.Size = new System.Drawing.Size(290, 63);
            this.btnFSTEK.TabIndex = 16;
            this.btnFSTEK.Text = "Сохранить файл с БДУ ФСТЭК";
            this.btnFSTEK.UseVisualStyleBackColor = true;
            this.btnFSTEK.Click += new System.EventHandler(this.btnFSTEK_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(921, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(183, 63);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Сохранить на диск";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // textBoxresult
            // 
            this.textBoxresult.AcceptsReturn = true;
            this.textBoxresult.Location = new System.Drawing.Point(598, 92);
            this.textBoxresult.MaximumSize = new System.Drawing.Size(700, 601);
            this.textBoxresult.Multiline = true;
            this.textBoxresult.Name = "textBoxresult";
            this.textBoxresult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxresult.Size = new System.Drawing.Size(579, 601);
            this.textBoxresult.TabIndex = 14;
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(265, 44);
            this.numericUpDown2.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown2.TabIndex = 13;
            this.numericUpDown2.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(11, 43);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown1.TabIndex = 12;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // gridBDU
            // 
            this.gridBDU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridBDU.Location = new System.Drawing.Point(11, 92);
            this.gridBDU.MultiSelect = false;
            this.gridBDU.Name = "gridBDU";
            this.gridBDU.RowTemplate.Height = 28;
            this.gridBDU.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridBDU.Size = new System.Drawing.Size(562, 601);
            this.gridBDU.TabIndex = 11;
            this.gridBDU.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridBDU_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "Номер страницы";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 20);
            this.label2.TabIndex = 18;
            this.label2.Text = "Количество строк";
            // 
            // FormBDU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1193, 723);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFSTEK);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.textBoxresult);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.gridBDU);
            this.Name = "FormBDU";
            this.Text = "БДУ ФСТЭК";
            this.Load += new System.EventHandler(this.FormBDU_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBDU)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFSTEK;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox textBoxresult;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.DataGridView gridBDU;
    }
}

