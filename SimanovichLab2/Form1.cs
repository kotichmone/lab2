﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using System.Collections;

using Excel = Microsoft.Office.Interop.Excel;
using System.Net;

namespace SimanovichLab2
{
    public partial class FormBDU : Form
    {
        public static ArrayList listOfAllThreats = new ArrayList(); //массив для хранения угроз
        public static ArrayList listnewThreats = new ArrayList(); //массив для хранения угроз из обновленного файла
        public int countofpage = 1; //переменная, означающая номер страниц
        public int maxcountofpage = 20; //переменная, означающая максимальное количество номеров страниц
        public int countofvaluesinpage = 15; //переменная, означающая минимальное количество строк на странице
        public int maxcountofvaluesinpage = 100; //переменная, означающая максимальное количество строк на странице

        public FormBDU()
        {
            InitializeComponent();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e) //изменение номера страницы
        {
            numericUpDown1.Maximum = maxcountofpage - 1;
            int currentvalueofdomain = Convert.ToInt32(numericUpDown1.Value);
            Print(currentvalueofdomain, countofvaluesinpage); //вывод угроз
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e) //изменения количества угроз на странице
        {
            
            int currentvalueofquantitative = Convert.ToInt32(numericUpDown2.Value); //количество элементов
            int currentvalueofdomain = Convert.ToInt32(numericUpDown1.Value); //номер страниц
            countofvaluesinpage = currentvalueofquantitative;
            maxcountofpage = listOfAllThreats.Count / countofvaluesinpage + 2; //изменение количества страниц
            numericUpDown1.Maximum = maxcountofpage - 1;
            Print(currentvalueofdomain, countofvaluesinpage); //вывод угроз
        }

        private void gridBDU_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBoxresult.Text = ""; //очистка поля вывода текста
            int selRowNum = gridBDU.CurrentCell.RowIndex; //номер выбранного элемента
            int currentvalueofquantitative = Convert.ToInt32(numericUpDown2.Value); //количество элементов на страинце
            int currentvalueofdomain = Convert.ToInt32(numericUpDown1.Value); //количество страниц
            int currentid = (currentvalueofdomain - 1) * currentvalueofquantitative + selRowNum; //идентификационный номер угрозы
            Threat thisthreat = (Threat)listOfAllThreats[currentid];
            textBoxresult.Text = "Идентификатор угрозы" + Environment.NewLine + "УБИ " + thisthreat.id + Environment.NewLine + Environment.NewLine; //вывод сведений об выбранной угрозе 
            textBoxresult.Text += "Наименование угрозы" + Environment.NewLine + thisthreat.name + Environment.NewLine + Environment.NewLine;
            textBoxresult.Text += "Описание угрозы " + Environment.NewLine + thisthreat.value + Environment.NewLine + Environment.NewLine;
            textBoxresult.Text += "Источник угрозы " + Environment.NewLine + thisthreat.Source + Environment.NewLine + Environment.NewLine;
            textBoxresult.Text += "Объект воздействия угрозы " + Environment.NewLine + thisthreat.ObjectThreat + Environment.NewLine + Environment.NewLine;
            if (thisthreat.Сonfidentiality == true)
            {
                textBoxresult.Text += "Нарушение конфиденциальности" + Environment.NewLine;
            }
            if (thisthreat.Integrity == true)
            {
                textBoxresult.Text += "Нарушение целостности" + Environment.NewLine;
            }
            if (thisthreat.Availability == true)
            {
                textBoxresult.Text += "Нарушение доступности" + Environment.NewLine;
            }
        }

        private void btnFSTEK_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxresult.Text = "";
                new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", "threats.xlsx"); //получение файла с БДУ ФСТЭК
                textBoxresult.Text = "Файл с угрозами получен";
                textBoxresult.Text += Environment.NewLine + "Обновление записей";

                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"threats.xlsx");
                listnewThreats = GetExcelData(path); //получение данных из Excel файла в массив 
                if (listOfAllThreats.Count > 0)
                {
                    Check(); //метод проверки изменений добавленных значений с имеющимися 
                }

                listOfAllThreats = listnewThreats; //обновление значений 
                int currentvalueofquantitative = Convert.ToInt32(numericUpDown2.Value); //количество элементов на странице 
                int currentvalueofdomain = Convert.ToInt32(numericUpDown1.Value); //количество страниц
                Print(currentvalueofdomain, currentvalueofquantitative);
                textBoxresult.Text += Environment.NewLine + "Обновление записей прошло успешно";
            }
            catch (Exception)
            {
                textBoxresult.Text = "Невозможно получить файл с угрозами";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxresult.Text = "";
                //Объявляем приложение
                Excel.Application ex = new Microsoft.Office.Interop.Excel.Application();
                //Не отображать Excel
                ex.Visible =false;
                //Количество листов в рабочей книге
                ex.SheetsInNewWorkbook = 1;
                //Добавить рабочую книгу
                Excel.Workbook workBook = ex.Workbooks.Add(Type.Missing);
                //Отключить отображение окон с сообщениями
                ex.DisplayAlerts = false;
                //Получаем первый лист документа (счет начинается с 1)
                Excel.Worksheet sheet = (Excel.Worksheet)ex.Worksheets.get_Item(1);
                //Название листа (вкладки снизу)
                sheet.Name = "Угрозы";
                //Объявление шапки 

                sheet.Cells[1, 1] = String.Format("Идентификатор УБИ");
                sheet.Cells[1, 2] = String.Format("Наименование УБИ");
                sheet.Cells[1, 3] = String.Format("Описание");
                sheet.Cells[1, 4] = String.Format("Источник угрозы");
                sheet.Cells[1, 5] = String.Format("Объект воздействия");
                sheet.Cells[1, 6] = String.Format("Нарушение конфиденциальности");
                sheet.Cells[1, 7] = String.Format("Нарушение целостности");
                sheet.Cells[1, 8] = String.Format("Нарушение доступности");
                sheet.Cells[1, 9] = String.Format("Дата включения угрозы в БнД УБИ");
                sheet.Cells[1, 10] = String.Format("Дата последнего изменения данных");
                int countofthreats = listOfAllThreats.Count;
                for (int i = 0; i < (countofthreats - 2); i++)
                {
                    
                    Threat t = (Threat)listOfAllThreats[i];
                    sheet.Cells[i + 2, 1] = String.Format(t.id.ToString());
                    sheet.Cells[i + 2, 2] = String.Format(t.name.ToString());
                    sheet.Cells[i + 2, 3] = String.Format(t.value.ToString());
                    sheet.Cells[i + 2, 4] = String.Format(t.Source.ToString());
                    sheet.Cells[i + 2, 5] = String.Format(t.ObjectThreat.ToString());
                    sheet.Cells[i + 2, 6] = String.Format(t.Сonfidentiality.ToString());
                    sheet.Cells[i + 2, 7] = String.Format(t.Integrity.ToString());
                    sheet.Cells[i + 2, 8] = String.Format(t.Availability.ToString());
                    sheet.Cells[i + 2, 9] = String.Format(t.Datastart.ToString());
                    sheet.Cells[i + 2, 10] = String.Format(t.Dataupdate.ToString());
                }

                string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"thrlistsave.xlsx");
                ex.Application.ActiveWorkbook.SaveAs(path, Type.Missing,
  Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange,
  Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing); //сохранение в Excel файл 
                textBoxresult.Text = "Файл успешно сохранен";

                ex.Quit();
            }

            catch (Exception exc)
            {

                textBoxresult.Text = exc.Message;
            }
        }

        private void FormBDU_Load(object sender, EventArgs e)
        {
            textBoxresult.Text = "Здравствуйте! Вас приветствует Парсер информации из официального банка данных угроз ФСТЭК России" + Environment.NewLine+"Для просмотра подробной информации об угрозе кликните на строку с угрозой" + Environment.NewLine+"Для увеличения количества выводимых строк и перелистыванию страниц поменяйте значения вверху страницы";
            try
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"thrlist.xlsx"); //путь до файла, хранящего данные БДУ ФСТЭК
                listOfAllThreats = GetExcelData(path); //получение данных из файла в массив
                gridBDU.Columns.Add("ID", "Идентификатор"); //добавление колонки идентификатора
                maxcountofpage = listOfAllThreats.Count / countofvaluesinpage + 2; //получение максимального количества листов
                gridBDU.Columns.Add("Name", "Название УБИ          "); //добавление колонки названия УБИ
                

                for (int i = 0; i < countofvaluesinpage; i++)
                {
                    Threat t = (Threat)listOfAllThreats[i];
                    gridBDU.Rows.Add("УБИ " + t.id, t.name); //начальный экран вывода угроз
                }
            }
            catch (Exception ex)
            {
                textBoxresult.Text = "Файл с угрозами не найден. Скачайте файл с БДУ ФСТЭК" + Environment.NewLine + ex.Message; ;
            }
        }




        public static ArrayList GetExcelData(string fullPath)
        {
            Microsoft.Office.Interop.Excel.Application excelapp = new Microsoft.Office.Interop.Excel.Application();
            excelapp.Visible = false;

            Microsoft.Office.Interop.Excel.Workbook excelappworkbook = excelapp.Workbooks.Open(
                fullPath,
                Type.Missing, Type.Missing, true, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing);
            Microsoft.Office.Interop.Excel.Worksheet excelworksheet = (Microsoft.Office.Interop.Excel.Worksheet)excelappworkbook.Worksheets.get_Item(1);
            Microsoft.Office.Interop.Excel.Range excelcells = excelworksheet.UsedRange;

            int rowsNum = 0;
            ArrayList allList = new ArrayList();

            for (rowsNum = 3; rowsNum != excelcells.Rows.Count; rowsNum++) //чтение с третьей строки 
            {
                double id = ((excelcells.Cells[rowsNum, 1] as Microsoft.Office.Interop.Excel.Range).Value2);
                string name = (((excelcells.Cells[rowsNum, 2] as Microsoft.Office.Interop.Excel.Range).Value2));
                string value = Convert.ToString(((excelcells.Cells[rowsNum, 3] as Microsoft.Office.Interop.Excel.Range).Value2));
                string sourse = Convert.ToString(((excelcells.Cells[rowsNum, 4] as Microsoft.Office.Interop.Excel.Range).Value2));
                string objecthreat = Convert.ToString(((excelcells.Cells[rowsNum, 5] as Microsoft.Office.Interop.Excel.Range).Value2));
                bool confidentiality = false;
                string conf = Convert.ToString(((excelcells.Cells[rowsNum, 6] as Microsoft.Office.Interop.Excel.Range).Value2));
                if (conf == "1")
                {
                    confidentiality = true;
                }

                bool integrity = false;
                string integr = Convert.ToString(((excelcells.Cells[rowsNum, 7] as Microsoft.Office.Interop.Excel.Range).Value2));
                if (integr == "1")
                {
                    integrity = true;
                }
                bool availability = false;
                string avail = Convert.ToString(((excelcells.Cells[rowsNum, 8] as Microsoft.Office.Interop.Excel.Range).Value2));
                if (avail == "1")
                {
                    availability = true;
                }
                string datastart = Convert.ToString(((excelcells.Cells[rowsNum, 9] as Microsoft.Office.Interop.Excel.Range).Value2));
                string dataupdate = Convert.ToString(((excelcells.Cells[rowsNum, 10] as Microsoft.Office.Interop.Excel.Range).Value2));
                Threat t = new Threat(id, name, value, sourse, objecthreat, confidentiality, integrity, availability, datastart, dataupdate); //создание угрозы 

                allList.Add(t); //добавление каждой угрозы в массив
            }


            excelappworkbook.Close(false, Type.Missing, Type.Missing);
            excelapp.Quit();

            return allList;
        }



        private void Print(int currentvalueofdomain, int countofvaluesinpage) //метод отображения угроз
        {
            gridBDU.Rows.Clear(); //очистка вывода от предыдущих значений
            gridBDU.Columns.Clear();
            if ((0 < currentvalueofdomain) && (currentvalueofdomain < maxcountofpage))
            {
                gridBDU.Columns.Add("ID", "Идентификатор УБИ");

                gridBDU.Columns.Add("Name", "Название УБИ");
                for (int i = (currentvalueofdomain - 1) * countofvaluesinpage; i < (currentvalueofdomain) * countofvaluesinpage; i++)
                {
                    if (i < listOfAllThreats.Count) //проверка окончания списка угроз
                    {
                        Threat t = (Threat)listOfAllThreats[i];
                        gridBDU.Rows.Add("УБИ " + t.id, t.name);
                    }

                }
            }
        }


        private void Check()
        {
            for (int i = 0; i < listnewThreats.Count; i++)
            {

                Threat t1 = (Threat)listnewThreats[i]; //новая угроза
                Threat t2 = (Threat)listOfAllThreats[i]; //старая угроза 
                if (!(t1.id == t2.id) && (t1.name == t2.name) && (t1.value == t2.value) && (t1.Сonfidentiality == t2.Сonfidentiality) && (t1.Datastart == t2.Datastart) && (t1.Dataupdate == t2.Dataupdate) && (t1.Source == t2.Source) && (t1.ObjectThreat == t2.ObjectThreat) && (t1.Integrity == t2.Integrity) && (t1.Availability == t2.Availability))
                {
                    textBoxresult.Text += Environment.NewLine + "БЫЛО:  УБИ " + t2.id;

                    if (t1.id != t2.id)
                    {
                        textBoxresult.Text += Environment.NewLine + "CТАЛО: УБИ " + t1.id;
                    }
                    if (t1.name != t2.name)
                    {
                        textBoxresult.Text += Environment.NewLine + "Наименование  " + t2.name + Environment.NewLine + "CТАЛО: УБИ " + t1.id + "Наименование  " + t1.name;
                    }
                    if (t1.value != t2.value)
                    {
                        textBoxresult.Text += Environment.NewLine + "Описание угрозы  " + t2.value + Environment.NewLine + "CТАЛО: УБИ " + t1.id + "Описание угрозы  " + t1.value;
                    }
                    if (t1.Сonfidentiality != t2.Сonfidentiality)
                    {
                        textBoxresult.Text += Environment.NewLine + "Нарушение конфиденциальности  " + t2.Сonfidentiality + Environment.NewLine + "CТАЛО: УБИ " + t1.id + "Нарушение конфиденциальности  " + t1.Сonfidentiality;
                    }
                    if (t1.Source != t2.Source)
                    {
                        textBoxresult.Text += Environment.NewLine + "Источник угрозы  " + t2.Source + Environment.NewLine + "CТАЛО: УБИ " + t1.id + "Источник угрозы  " + t1.Source;
                    }
                    if (t1.ObjectThreat != t2.ObjectThreat)
                    {
                        textBoxresult.Text += Environment.NewLine + "Объект воздействия угрозы  " + t2.ObjectThreat + Environment.NewLine + "CТАЛО: УБИ " + t1.id + "Объект воздействия угрозы  " + t1.ObjectThreat;
                    }
                    if (t1.Availability != t2.Availability)
                    {
                        textBoxresult.Text += Environment.NewLine + "Нарушение конфиденциальности  " + t2.Availability + Environment.NewLine + "CТАЛО: УБИ " + t1.id + "Нарушение конфиденциальности  " + t1.Availability;
                    }
                    if (t1.Integrity != t2.Integrity)
                    {
                        textBoxresult.Text += Environment.NewLine + "Нарушение доступности  " + t2.Integrity + Environment.NewLine + "CТАЛО: УБИ " + t1.id + "Нарушение доступности  " + t1.Integrity;
                    }
                    if (t1.Datastart != t2.Datastart)
                    {
                        textBoxresult.Text += Environment.NewLine + "Дата включения угрозы в БнД УБИ  " + t2.Datastart + Environment.NewLine + "CТАЛО: УБИ " + t1.id + "Дата включения угрозы в БнД УБИ  " + t1.Datastart;
                    }
                    if (t1.Dataupdate != t2.Dataupdate)
                    {
                        textBoxresult.Text += Environment.NewLine + "Дата последнего изменения данных  " + t2.Dataupdate + Environment.NewLine + "CТАЛО: УБИ " + t1.id + "Дата последнего изменения данных  " + t1.Dataupdate;
                    }
                    textBoxresult.Text += Environment.NewLine;
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
